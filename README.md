# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ: Существет три способа сменить контекст вызова: bind, call и apply.
> 1) Bind позволяет функции записать вместо this и запомнить то окружение, которое будет передано в качестве первого аргумента. Последующие аргументы будут переданы в функцию в качестве параметров.
> 2) Call позволяет задать окружение и сразу же вызвать обновленную функцию с необходимыми аргументами, перечисленными через запятую после названия окружения.
> 3) Apply полностью повторяет функционал Call и отличается лишь тем, что аргументы передаются в виде массива. 
#### 2. Что такое стрелочная функция?
> Ответ: Стрелочная функция - это аналогичная запись function expression. Их используют благодаря их краткости и отсутвия собственного свойства this, из-за этого контекст берется из окружения.
#### 3. Приведите свой пример конструктора. 
```js
function Car(brand, model, color) {
  this.brand = brand;
  this.model = model;
  this.color = color;
  this.getBrand = () => this.brand;
  this.getModel = () => this.model;
  this.getColor = () => this.color;
}

const myCar = new Car('volkswagen', 'golf', 'gray');
```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

* Способ 1
```js
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }.bind(person), 1000)
    }
  }

  person.sayHello();
```

* Способ 2
```js
  const person = {
    name: 'Nikita',
    sayHello: function() {
      personThis = this;
      setTimeout(function() {
          console.log(personThis.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();
```

* Способ 3
```js
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(() => {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();
```

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
